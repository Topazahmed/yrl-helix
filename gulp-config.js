module.exports = function (env) {
	
	
	var instanceRootVars = {
		"default" : "C:\\fff-deploy",
//		"dev" : "C:\\inetpub\\wwwroot\\fff-helix.getfishtank.ca",
		"dev" : "C:\\fff-deploy",
		"qa" : "C:\\fff-deploy",
		"prod" : "C:\\fff-deploy",
	};		
	
	var buildConfigurationVars = {
		"default" : "Release",
		"dev" : "Debug",
		"qa" : "Release",
		"prod" : "Release",
	};		
	
			
	var instanceRoot = instanceRootVars[env], 
		artifactRoot = "c:\\fff-artifact",
		websiteRoot = instanceRoot;				
  
  var config = {    
	
	cssPaths : ['./src/Project/Website/code/**/*.scss', '!./src/**/obj/**/*.scss', '!./src/Project/**/_*.scss'],
	jsPaths :  ['./src/Project/Website/code/**/*.js', '!./src/**/obj/**/*.js', '!./src/Project/**/_*.js'],
	untransformedConfigPaths : ["./src/Foundation/**/*.config",
								"./src/Feature/**/*.config",
								"./src/Project/**/*.config"],
	assembliesSyntax: "{Fishtank,FFF}.*.{dll,pdb}",
	
	
	websiteRoot: websiteRoot,
	artifactRoot : artifactRoot,	
    sitecoreLibraries: instanceRoot + "\\Website\\bin",
    //licensePath: instanceRoot + "\\App_Data\\license.xml",
    licensePath: websiteRoot + "\\App_Data\\license.xml",
	updatePackagePaths : ['./src/Project/**/*.update'],
	updatePackageDestinationFolder : websiteRoot + "\\App_Data\\SitecorePackageDeployer",
    
	solutionName: "Website",
   	buildConfiguration: buildConfigurationVars[env],
   	buildToolsVersion: 15.0,
   	buildMaxCpuCount: 0,
   	buildVerbosity: "minimal",
   	buildPlatform: "Any CPU",
   	publishPlatform: "AnyCpu",
   	runCleanBuilds: false,
	
	octopusServer: "http://localhost/octopus",
	octopusApiKey: "API-MPTYMH0NHQBKPF6XYHFQMMWZP80",
	
  };
  return config;
}

﻿#Custom Not Found

This Helix structured project is intended to provide customizable 404 page functionality. Include this project in your Helix solution and set as described below.

In your App_Config/Include/SiteDefinition.config file:

```xml
<sites>
	<site name="siteName" patch:before="site[@name='website']"
        hostName="siteName.getfishtank.ca|*.azurewebsites.net"
        targetHostName ="siteName.getfishtank.ca" 
        contentStartItem="/sitecore/content/siteName"
        virtualFolder="/"
        physicalFolder="/"
        rootPath="/sitecore/content/siteName" /* Ensure this is correct */
        startItem="/home" /* Ensure this is correct */
        database="web"
        domain="extranet"
        allowDebug="true"
        cacheHtml="true"
        htmlCacheSize="50MB"
        enablePreview="true"
        enableWebEdit="true"
        enableDebugger="true"
        disableClientData="false"
        notFoundPagePath="/404" /* Add the name of your 404 page Sitecore item with a "/" prepended */
	/>
</sites>
```
The path to your page item is resolved in Page404Resolver.cs as follows:

```c#
var startItemPath = Context.Site.StartPath; // Resolves to rootPath + startItem
var notFoundPageItem = Context.Database.GetItem(startItemPath + notFoundPagePath);
```

The final path is `rootPath + startItem + notFoundPagePath`. When your site definiton is set up as instructed, add the 404 page to your Sitecore tree. Build, set workflow, and publish. All should be working!
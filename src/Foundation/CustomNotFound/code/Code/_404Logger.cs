﻿using System.Net;
using Sitecore.Pipelines.HttpRequest;
using System.Web;
using log4net;
using Sitecore.Diagnostics;

namespace Fishtank.Foundation.CustomNotFound.Code
{
    public static class _404Logger
    {
        public static ILog Log => LogManager.GetLogger("CustomErrors._404Logger") ?? LoggerFactory.GetLogger(typeof(_404Logger));

        public static string Fmt(this string fmt, params object[] args)
        {
            return string.Format(fmt, args);
        }
    }

}

﻿using System;
using System.Net;
using Sitecore.Pipelines.HttpRequest;
using System.Web;
using Sitecore.Configuration;

namespace Fishtank.Foundation.CustomNotFound.Code
{
    public class Set404StatusCode : HttpRequestBase
    {
        protected override void Execute(HttpRequestArgs args)
        {
            // retain 500 response if previously set
            if (HttpContext.Current.Response.StatusCode >= 500 || HttpContext.Current.Request.RawUrl == "/")
                return;

            // return if request does not end with value set in ItemNotFoundUrl, i.e. successful page
            if (!HttpContext.Current.Request.Url.LocalPath.EndsWith(Settings.ItemNotFoundUrl, StringComparison.InvariantCultureIgnoreCase))
                return;

            _404Logger.Log.Warn("Page Not Found: " + HttpContext.Current.Request.RawUrl + ", current status: " + HttpContext.Current.Response.StatusCode);
            HttpContext.Current.Response.TrySkipIisCustomErrors = true;
            HttpContext.Current.Response.StatusCode = (int)HttpStatusCode.NotFound;
            HttpContext.Current.Response.StatusDescription = "Page not found";
        }
    }
}

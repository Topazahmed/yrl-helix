﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['cash'], factory);
    } else {
        // Browser globals
        root.FFUtil = factory(root.Cash);
    }
}(this, function ($) {
    
    var FFUtil = function(options){

        // Variables
        var defaulOptions = {};
        
        // Private
        /* */
        var getTransitionEndEventName = function(){
            var t,
                el = document.createElement("fakeelement");

            var transitions = {
                "transition": "transitionend",
                "OTransition": "oTransitionEnd",
                "MozTransition": "transitionend",
                "WebkitTransition": "webkitTransitionEnd"
            }

            for (t in transitions) {
                if (el.style[t] !== undefined) {
                    return transitions[t];
                }
            }
            return "";
        };

        // Listeners
        /* */

        // Virtual Constructor;
        (function(opts){        
            options = $.extend({}, defaulOptions, opts);
        })(options);


        // Public 
        return {
            "getTransitionEndEventName" : function () { return getTransitionEndEventName(); }            
        };

    };
    
    FFUtil.constructor = FFUtil;
    return FFUtil;

}));
﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['cash', 'lib/open-close', 'lib/eventer'], factory);
    } else {
        // Browser globals
        root.MobileNav = factory(root.Cash, root.OpenClose, root.Eventer);
    }
}(this, function ($, OpenClose, Eventer) {

    var MobileNav = function (options) {

        // Variables =========================================================

        var defaultOptions = {
            "selectors": {
                "openButton": ".mobile-menu-open",
                "closeButton": ".mobile-menu-close",
                "mobileMenuPanel": ".mobile-menu-panel",
                "openMobileMenu": ".mobile-menu-is-open"
            },
            "class": {
                "openState": ""
            },
            "events": {
                "open": "mobile-menu-open",
                "opening": "mobile-menu-opening",
                "opened": "mobile-menu-opened",
                "close": "mobile-menu-close",
                "closing": "mobile-menu-closing",
                "closed": "mobile-menu-closed",
                "toggle": "mobile-menu-toggle",
            }
        },
            $html = $('html'),
            $body = $html.find('body'),
            $window = $(window),
            lockedState = false; // Non dom related events

        var eventer;
        var openClose;

        // Private =========================================================
        var open = function () {
            openClose.open();
        };

        var close = function () {
            openClose.close();
        };

        var toggle = function () {
            openClose.toggle();
        }


        // Event Handlers =========================================================
        // Latent binding of events after options have been sorted out
        var bindEvents = function () {

            $window.on(options.events.close, function (e) {
                eventer.fire(openClose.getEvents().close);
            });

            $window.on(options.events.open, function (e) {
                eventer.fire(openClose.getEvents().open);
            });

            $window.on(options.events.toggle, function (e) {
                eventer.fire(openClose.getEvents().toggle);
            });

            //$window.on(openClose.getEvents().toggle, function(e) {});
            $window.on(openClose.getEvents().open, function (e) { eventer.fire(options.events.open); });
            $window.on(openClose.getEvents().opening, function (e) { eventer.fire(options.events.opening);  });
            $window.on(openClose.getEvents().opened, function (e) { eventer.fire(options.events.opened);  });

            $window.on(openClose.getEvents().close, function (e) { eventer.fire(options.events.close); });
            $window.on(openClose.getEvents().closing, function (e) { eventer.fire(options.events.closing);  });
            $window.on(openClose.getEvents().closed, function (e) { eventer.fire(options.events.closed); });

            // Close the menu when clicking anything other than the menu
            $html.on("click", "*", function () {
                openClose.close();
                //eventer.fire(openClose.getEvents().close);

                // todo: You can do a event.preventDefault() during the touchstart event to cancel the click events.
                // return false -- Mobile fix? todo: fix bug where mobile taps don't work
            });

            $body.on('click', options.selectors.openButton, function (e) {
                e.preventDefault();

                var el = document.querySelector(options.selectors.mobileMenuPanel);
                el.scrollTop = 0;

                openClose.open();

                //eventer.fire(openClose.getEvents().open);
            });

            $body.on('click', options.selectors.closeButton, function (e) {
                e.preventDefault();
                openClose.close();

                //eventer.fire(openClose.getEvents().close);
            });

            // Menu close on ESC key
            $body.on('keyup', function (e) {
                if (e.keyCode == 27) {
                    openClose.close();
                    //eventer.fire(openClose.getEvents().close);
                }
            });

            // When clicking on the mobile panel or the mobile toggle button, do not propogate the click event, or else other events will be incorrectly fired
            $body.on("click", options.selectors.mobileMenuPanel + ", " + options.selectors.openButton, function (e) {
                e.stopPropagation();
            });

        };


        // Virtual Constructors =======================
        (function (opts) {
            // TODO - insert validation for required elements
            // santizie the "open" selector to double as the active class state            

            options = $.extend({}, defaultOptions, opts);

            openClose = new OpenClose(
                {
                    "selectors": {
                        "openClosePanel": options.selectors.mobileMenuPanel
                    },
                    "class": {
                        "openState": options.class.openState
                    }
                }
            );

            eventer = new Eventer();
            bindEvents();

        })(options);

        // Public Functions =========================================================
        var pub = {};
        pub.open = function () { open(); };
        pub.close = function () { close(); };
        pub.toggle = function () { toggle(); };
        return pub;
    };

    MobileNav.constructor = MobileNav;
    return MobileNav;

}));

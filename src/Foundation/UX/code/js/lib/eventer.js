﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['cash'], factory);
    } else {
        // Browser globals
        root.Eventer = factory(root.c$);
    }
}(this, function ($) {
    
    var Eventer = function(options){

        // Variables
        var defaulOptions = { "events" : null };

        // Virtual Constructor;
        (function(opts){        
            options = $.extend({}, defaulOptions, opts);
        })(options);

        // Private
        var fire = function (event, data, $elem) {
            $($elem || window).trigger(
                // jQuery
                // $.Event(event, { "args": data })
                // Cash
                event, {"args" : data}
            );
        };

        // Listeners
        /* */

        // Public 
        return {
            "fire" : function (event, data, $elem) {
                fire(event, data, $elem);
            },
            "events" : options.events
        };

    };
    
    return Eventer;

}));
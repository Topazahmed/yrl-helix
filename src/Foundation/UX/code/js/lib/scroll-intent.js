﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['cash', 'lib/eventer'], factory);
    } else {
        // Browser globals
        root.ScrollIntent = factory(root.c$, root.Eventer);
    }
}(this, function ($, Eventer) {
    
    var ScrollIntent = function(options){

        // Variables
        // ====
        var $window = $(window),
            htmlEl = document.getElementsByTagName('html')[0],
            eventer,
            lastDirection = "none";            
            
        var scrollTopOffset = 0,
            deadzoneShow = 5,
            deadzoneHide = 5,
            topOfPage = 0,
            topOfPageDeadzone = {"top": 0, "bottom": 0};

        var lastY = null;
            
        var events = {
                "movingUp" : "movingUp",
                "movingDown" : "movingDown",
            };
        var defaulOptions = {}; // not used, legacy variable aboves

        // Private
        // ====

        var getCurrentY = function() {
            return htmlEl.getBoundingClientRect().top * -1;
        };

        var getPageHeight = function(){
            return htmlEl.scrollHeight;
        };

        var getViewportHeight = function(){
            return window.outerHeight;
        };

        var getMaxScrollY = function(){
            return getPageHeight() - getViewportHeight();
        };

        var isTopPageDeadzone = function() {
            var y = getCurrentY();
            return (y >= topOfPageDeadzone.top && y <= topOfPageDeadzone.bottom);    
        };

        var checkIntent = function () {
                   
            var scrollY = getCurrentY();
            if(lastY == null) lastY = getCurrentY();

            if(isTopPageDeadzone()){
                // do nothing                
            }            
            else if (scrollY === topOfPage || scrollY < topOfPage) {
                // at top
                hasIntentChanged('up') && eventer.fire(events.movingUp);
            }
            else if (scrollY >= getMaxScrollY()) {
                // at bottom
                hasIntentChanged('down') && eventer.fire(events.movingDown);
            }
            else if (scrollY - deadzoneHide > lastY) {
                // scrolling down
                hasIntentChanged('down') && eventer.fire(events.movingDown);
            }
            else if (scrollY + deadzoneShow < lastY) {
                // scrolling up
                hasIntentChanged('up') && eventer.fire(events.movingUp);
            }
            lastY = scrollY;
        };

        var hasIntentChanged = function(direction) {

            

            if (direction != lastDirection) {
                lastDirection = direction;
                return true;
            }
            return false;
        };

        // Listeners
        // ====
        $window.on('scroll', function (e) {
            checkIntent();
        });

        // Virtual Constructor;
        // ====
        (function(opts){        
            eventer = new Eventer();
            
            options = $.extend({}, defaulOptions, opts);
            topOfPageDeadzone = options["topOfPageDeadzone"] || topOfPageDeadzone;            
            scrollTopOffset = options["scrollTopOffset"] || scrollTopOffset;
            deadzoneShow = options["deadzoneShow"] || deadzoneShow;
            deadzoneHide = options["deadzoneHide"] || deadzoneHide;
            topOfPage = options["topOfPage"] || topOfPage;

            checkIntent();
        })(options);

        // Public 
        // ====
        return {
            "scrollTopOffset" : function (offsetY) {
                scrollTopOffset = offsetY;
            },
            "events" : events
        };

    };
    
    return ScrollIntent;
}));
﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['cash', 'lib/eventer', 'lib/ff-util'], factory);
    } else {
        // Browser globals
        root.OpenClose = factory(root.Cash, root.Eventer, root.FFUtil);
    }
}(this, function ($, Eventer, FFUtil) {
    
    var OpenClose = function (options) {
        
        // Variables =========================================================
        var defaultOptions = {
            "selectors" : {
                "openClosePanel" : ".open-close-panel"
            },
            "class" : {
                "openState" : "open-state"
            },
            "events" : { 
                "open" : "opopen-",
                "opening" : "opopening-",
                "opened" : "opopened-",                
                "close" : "opclose-",
                "closing" : "opclosing-",
                "closed" : "opclosed-",
                "toggle" : "optoggle-"
            }
        };
        var 
            ffUtil = new FFUtil(),
            eventer = new Eventer(),
            $html = $('html'),
            $body = $html.find('body'),
            $window = $(window),            
            $openClosePanel = $body.find(options.selectors.openClosePanel),
            lockedState = false // Non dom related events
            ;
            

        // Tools =========================================================
        var transitionEndEventNameString = ffUtil.getTransitionEndEventName();

        // Private =========================================================
        var getRandom = function(){
            return Math.ceil(Math.random()*100000).toString();
        };
        
        var isLocked = function () {
            return lockedState;
        };

        var isOpen = function () {
            return $body.hasClass(options.class.openState);
        };

        var isClosed = function () { return !isOpen(); };

        var open = function () {
            if (isLocked()) return;
            if (isOpen()) return;
            
            lockedState = true;
            $body.addClass(options.class.openState);
            eventer.fire(options.events.opening);

            $openClosePanel.one(transitionEndEventNameString,
                       function (event) {
                           eventer.fire(options.events.opened);
                           lockedState = false;
                           event.stopPropagation();
                       });
        };

        var close = function () {
            if (isLocked()) return;
            if (isClosed()) return;

            lockedState = true;
            $body.removeClass(options.class.openState);
            eventer.fire(options.events.closing);

            $openClosePanel.one(transitionEndEventNameString,
                       function (event) {
                           eventer.fire(options.events.closed);
                           lockedState = false;
                           event.stopPropagation();
                       });
        };

        var toggle = function () {
            isOpen() ? close() : open();
        }

        // Event Handlers =========================================================
        var eventBinding = function(){
            // REFACTOR 
            $window.on(options.events.close, function (e) {
                close();
            });
            // REFACTOR 
            $window.on(options.events.open, function (e) {
                open();
            });
            // REFACTOR 
            $window.on(options.events.toggle, function (e) {
                toggle();
            });
        };

                   
        // Virtual Constructors =======================
        (function(opts){        
            // TODO - insert validation for required elements
            
            // Add a random, unique identifier to each event
            defaultOptions.events.open += getRandom();
            defaultOptions.events.opening += getRandom();
            defaultOptions.events.opened += getRandom();
            defaultOptions.events.close += getRandom();
            defaultOptions.events.closed += getRandom();
            defaultOptions.events.closing += getRandom();
            defaultOptions.events.toggle += getRandom();            
            
            options = $.extend({}, defaultOptions, opts);
            eventBinding();
        })(options);

        // Public Functions =========================================================
        var pub = {};
        pub.open = function () { open(); };
        pub.close = function () { close(); };
        pub.toggle = function () { toggle(); };
        pub.getEvents = function() { return options.events; };
        return pub;
    };
    
    OpenClose.constructor = OpenClose;
    return OpenClose;
    
}));
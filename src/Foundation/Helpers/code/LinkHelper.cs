﻿using System;
using System.Net;
using System.Text;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;

namespace Fishtank.Foundation.Helpers
{
    public static class LinkHelper
    {
        public static string GetUrl(this Item item)
        {
            return GetFormattedUrl(item, null);
        }

        public static string GetFormattedUrl(this Item item, string[] pathsToRemove)
        {
            var options = LinkManager.GetDefaultUrlOptions();
            options.AddAspxExtension = false;
            options.LowercaseUrls = true;
            if (item == null) return string.Empty;
            string url = LinkManager.GetItemUrl(item, options);

            return GetFormattedUrlString(url, pathsToRemove);
        }

        private static string GetFormattedUrlString(string url, string[] pathsToRemove)
        {
            if (String.IsNullOrWhiteSpace(url)) return string.Empty;

            if (pathsToRemove == null) return url;

            foreach (var path in pathsToRemove)
            {
                url = url.Replace(path, String.Empty);
            }

            return url;
        }

        private static string GetFormattedUrlString(string url)
        {
            return GetFormattedUrlString(url, null);
        }

        public static string GetImageUrl(this Item item, string field)
        {
            string url = string.Empty;

            if (item == null)
                return url;

            ImageField imgField = (item.Fields[field]);

            var options = new MediaUrlOptions { AbsolutePath = true };

            if (imgField?.MediaItem != null)
            {
                MediaItem image = new MediaItem(imgField.MediaItem);
                url = StringUtil.EnsurePrefix('/', HashingUtils.ProtectAssetUrl(MediaManager.GetMediaUrl(image, options)));
            }

            return url.Replace("sitecore/shell/", "");
        }

        public static string GetImageUrl(this Item item, string field, MediaUrlOptions options)
        {
            string url = string.Empty;

            if (item == null)
                return url;

            ImageField imgField = (item.Fields[field]);

            options.AbsolutePath = true;

            if (imgField != null && imgField.MediaItem != null)
            {
                MediaItem image = new MediaItem(imgField.MediaItem);
                url = StringUtil.EnsurePrefix('/', HashingUtils.ProtectAssetUrl(MediaManager.GetMediaUrl(image, options)));
            }

            return url;
        }

        public static string GetMediaUrl(Item item)
        {
            var url = HashingUtils.ProtectAssetUrl(MediaManager.GetMediaUrl(item));
            return GetFormattedUrlString(url);
        }

        public static string GetAnchor(LinkField link)
        {
            if (!string.IsNullOrEmpty(link.Anchor)) return "#" + link.Anchor;

            return string.Empty;
        }

        public static string AnchorHashSign([NotNull] this LinkField link)
        {
            if (!string.IsNullOrEmpty(link.Anchor)) return "#" + link.Anchor;

            return string.Empty;
        }

        public static string GetUrlFromLink(this Item item, string field)
        {
            return GetUrlFromLinkField(item, field);
        }
        
        public static string GetUrlFromLinkField(this Item item, string field)
        {
            if (String.IsNullOrWhiteSpace((field)) || item == null)
                return String.Empty;

            var linkField = (LinkField)item.Fields[field];

            if (linkField == null) return String.Empty;

            switch (linkField.LinkType.ToLower())
            {
                case "internal":
                    // Use LinkMananger for internal links, if link is not empty
                    //String urlStart = linkField.TargetItem != null ? GetInternalUrl(linkField.TargetItem) : GetInternalUrl(item);
                    var linkURL = new StringBuilder();

                    if (linkField.TargetItem != null)
                    {
                        linkURL.Append(linkField.TargetItem.GetUrl());
                    }

                    if (!String.IsNullOrEmpty(linkField.QueryString))
                    {
                        linkURL.Append(WebUtility.UrlDecode(linkField.QueryString));
                    }

                    linkURL.Append(GetAnchor(linkField));

                    return linkURL.ToString();
                //return linkField.TargetItem != null ? GetUrl(linkField.TargetItem) : string.Empty;
                case "media":
                    // Use MediaManager for media links, if link is not empty
                    return linkField.TargetItem != null ? GetMediaUrl(linkField.TargetItem) : string.Empty;
                case "external":
                    // Just return external links
                    return linkField.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty
                    return !string.IsNullOrEmpty(linkField.Anchor) ? "#" + linkField.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link
                    return linkField.Url;
                case "javascript":
                    // Just return javascript
                    return linkField.Url;
                default:
                    // Just please the compiler, this
                    // condition will never be met
                    return linkField.Url;
            }
        }
        
        public static string GetUrlFromLinkField(this Item item, string field, string[] pathsToRemove)
        {
            if (String.IsNullOrWhiteSpace((field)) || item == null)
                return String.Empty;

            var linkField = (LinkField)item.Fields[field];

            if (linkField == null) return String.Empty;

            switch (linkField.LinkType.ToLower())
            {
                case "internal":
                    // Use LinkMananger for internal links, if link is not empty
                    //String urlStart = linkField.TargetItem != null ? GetInternalUrl(linkField.TargetItem) : GetInternalUrl(item);
                    StringBuilder linkURL = new StringBuilder();

                    if (linkField.TargetItem != null)
                    {
                        linkURL.Append(linkField.TargetItem.GetFormattedUrl(pathsToRemove));
                    }

                    if (!string.IsNullOrEmpty(linkField.QueryString))
                    {
                        linkURL.Append(WebUtility.UrlDecode(linkField.QueryString));
                    }

                    linkURL.Append(linkField.AnchorHashSign());

                    return linkURL.ToString();
                //return linkField.TargetItem != null ? GetUrl(linkField.TargetItem) : string.Empty;
                case "media":
                    // Use MediaManager for media links, if link is not empty
                    return linkField.TargetItem != null ? linkField.TargetItem.GetFormattedMediaUrl(pathsToRemove) : string.Empty;
                case "external":
                    // Just return external links
                    return linkField.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty
                    return !string.IsNullOrEmpty(linkField.Anchor) ? "#" + linkField.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link
                    return linkField.Url;
                case "javascript":
                    // Just return javascript
                    return linkField.Url;
                default:
                    // Just please the compiler, this
                    // condition will never be met
                    return linkField.Url;
            }
        }
		
		public static string GetRelAttributesFromLink(this Item item, string field)
        {
            /*
             * When your page links to another page using target="_blank", the new page runs on the same
             * process as your page. If the new page is executing expensive JavaScript, your page's
             * performance may also suffer. 
             * target="_blank" is also a security vulnerability. The new page has access to your
             * window object via window.opener, and it can navigate your page to a different URL using
             * window.opener.location = newURL. 
             */

            var relAttr = String.Empty;

            if (String.IsNullOrWhiteSpace(field) || item == null)
            {
                return relAttr;
            }

            var linkField = (LinkField)item.Fields[field];

            if (linkField == null)
            {
                return relAttr;
            }

            if (GetLinkTarget(item, field).Contains("_blank") && !linkField.IsInternal && !linkField.IsMediaLink)
            {
                relAttr = "noopener";
            }

            return relAttr;
        }

        public static string GetFormattedMediaUrl(this Item item)
        {
            return item.GetFormattedMediaUrl(null);
        }

        public static string GetFormattedMediaUrl(this Item item, string[] pathsToRemove)
        {
            var url = HashingUtils.ProtectAssetUrl(MediaManager.GetMediaUrl(item));
            return GetFormattedUrlString(url, pathsToRemove);
        }

        public static string GetLinkTarget(this Item item, string field)
        {
            if (String.IsNullOrWhiteSpace(field) || item == null) return "_self"; // default browser value

            var linkField = (LinkField)item.Fields[field];

            return linkField?.Target != null && !String.IsNullOrWhiteSpace(linkField.Target) ? linkField.Target : "_self";
        }
		
		public static string GetLinkTitle(this Item item, string field)
        {
            if (String.IsNullOrWhiteSpace(field) || item == null) return "";

            var linkField = (LinkField)item.Fields[field];

            return linkField?.Title != null && !String.IsNullOrWhiteSpace(linkField.Title) ? linkField.Title : "";
        }

        public static string GetLinkStyleClass(string field, Item item)
        {
            if (String.IsNullOrWhiteSpace((field)) || item == null)
                return String.Empty;

            var linkField = (LinkField)item.Fields[field];

            if (linkField == null || String.IsNullOrEmpty(linkField.Class)) return String.Empty;

            return linkField.Class;
        }
    }
}

﻿using System;
using System.Linq;
using System.Web;
using Sitecore;
using Sitecore.ContentSearch.Linq.Common;
using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.StringExtensions;

namespace Fishtank.Foundation.Helpers
{
    public static class ViewHelper
    {
        public static string GetParameterDropLinkValue(string itemIDAsString, string fieldName = "value")
        {
            ID id = new ID();
            if (!ID.TryParse(itemIDAsString, out id)) {
                return String.Empty;
            }

            return GetParameterDropLinkValue(id, fieldName);
        }
        public static string GetParameterDropLinkValue(ID itemID, string fieldName = "value")
        {
            if(ID.IsNullOrEmpty(itemID) || string.IsNullOrWhiteSpace(fieldName)) return String.Empty;
            var item = ItemHelper.GetItemById(itemID);
            if (item == null) return String.Empty;

            return item[fieldName];
        }
        public static bool FieldHasValue(Item item, string fieldName)
        {
            return item != null && !string.IsNullOrWhiteSpace(fieldName) && !string.IsNullOrWhiteSpace(item[fieldName]);
        }
        public static bool FieldHasValue(string fieldName)
        {
            return fieldName != null && !string.IsNullOrWhiteSpace(fieldName);
        }

        public static bool FieldHasValue(HtmlString fieldValue)
        {
            return fieldValue != null && !string.IsNullOrWhiteSpace(fieldValue.ToHtmlString())
                                      && !string.IsNullOrEmpty(fieldValue.ToHtmlString());
        }

        public static bool ShouldDisplayField(Item item, string fieldName)
        {
            return FieldHasValue(item, fieldName) || IsEditMode();
        }

        public static bool ShouldDisplayField(string fieldName)
        {
            return FieldHasValue(fieldName) || IsEditMode();
        }

        public static bool ShouldDisplayField(HtmlString fieldValue)
        {
            return FieldHasValue(fieldValue) || IsEditMode();
        }

        public static bool ShouldDisplayPlaceholder(string placeholderName)
        {
            return HasRenderingInPlaceholder(placeholderName) || IsEditMode();
        }
        public static bool HasRenderingInPlaceholder(string placeholderName)
        {
            if (placeholderName.Equals(String.Empty)) return false;

            return GetNumberOfRenderingsInPlaceholder(placeholderName) > 0;
        }
        public static bool IsEditMode()
        {
            return Context.PageMode.IsExperienceEditor;
        }
        public static int GetNumberOfRenderingsInPlaceholder(string placeholder)
        {
            var renderingReferences = Context.Item.Visualization.GetRenderings(Context.Device, true);
            var renderingsInPlaceholder = renderingReferences.Where(r => r.Placeholder.EndsWith(placeholder, StringComparison.OrdinalIgnoreCase));

            return renderingsInPlaceholder.Count();
        }
		public static string GetSiteDefinitionValue(string keyName)
		{
			if (Context.Site == null || keyName.IsNullOrEmpty()) return String.Empty;

			return Context.Site.Properties[keyName];
		}
    }
}

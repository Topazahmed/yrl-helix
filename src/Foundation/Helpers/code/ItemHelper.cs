﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Resources.Media;
using Sitecore.Rules;

namespace Fishtank.Foundation.Helpers
{
    public static class ItemHelper
    {
        public static Item GetItemFromPath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }

            return Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath + path);
        }

        public static Item GetItemById(string id)
        {
            if (!ID.IsID(id))
            {
                return null;
            }

            var newId = new ID(id);

            return GetItemById(newId);
        }

        public static Item GetItemById(ID id)
        {
            if (id.IsNull) return null;

            return Sitecore.Context.Database.GetItem(id);
        }

        public static List<Item> GetItemChildrenByTemplate(Item item, string templateId)
        {
            if (item == null || !ID.IsID(templateId))
            {
                return new List<Item>();
            }

            return GetItemChildrenByTemplate(item, new ID(templateId));
        }

        public static List<Item> GetItemChildrenByTemplate(Item item, ID templateId)
        {
            var emptyList = new List<Item>();

            if (item == null || templateId.IsNull || templateId.IsGlobalNullId)
            {
                return emptyList;
            }

            var children = item.GetChildren().Where(x => x.TemplateID.Equals(templateId)).ToList();

            if (children.Count == 0)
            {
                return emptyList;
            }

            return children;
        }

        public static List<Item> GetItemSiblings(Item item)
        {
            var emptyList = new List<Item>();

            if (item == null) return emptyList;

            var itemParent = item.Parent;

            if (itemParent == null) return emptyList;

            return itemParent.GetChildren().Where(i => i.ID != item.ID).ToList();
        }

        public static Item GetItemFromReferenceField(string field, Item item)
        {
            if (item?.Fields[field] == null) return null;

            ReferenceField reference = item.Fields[field];

            return reference?.TargetItem;
        }

        public static List<Item> GetItemsFromListField(string field, Item listItem)
        {
            var emptyList = new List<Item>();

            MultilistField multiField = listItem?.Fields[field];

            var items = multiField?.GetItems();

            if (items == null) return emptyList;
            if (items.Length == 0) return emptyList;

            return items.ToList();
        }

        public static Item GetItemFromRenderingField(RenderingParameters renderingParameters, string fieldName)
        {
            var itemId = renderingParameters?[fieldName];

            if (itemId == null || !ID.IsID(itemId)) return null;

            var id = new ID(itemId);

            var item = Sitecore.Context.Database.GetItem(id);

            return item;
        }

        public static string GetImageAltText(this Item item, string field)
        {
            if (item == null) return String.Empty;

            ImageField imgField = item.Fields[field];

            if (imgField == null) return String.Empty;

            return imgField.Alt;
        }

        public static Item GetRenderingItem()
        {
            return RenderingContext.Current?.Rendering?.Item;
        }

        public static List<Item> GetItemsByQuery(string query)
        {
            //TODO: add XPath validation
            var emptyList = new List<Item>();

            if (string.IsNullOrEmpty(query))
            {
                return emptyList;
            }

            var items = Sitecore.Context.Database.SelectItems(query);

            if (items == null || items.Length == 0)
            {
                return emptyList;
            }

            return items.ToList();
        }

        public static List<Item> GetItemsByFastQuery(string query)
        {
            var emptyList = new List<Item>();

            if (string.IsNullOrEmpty(query))
            {
                return emptyList;
            }

            if (!query.StartsWith("fast:"))
            {
                query = "fast:" + query;
            }

            var items = Sitecore.Context.Database.SelectItems(query);

            if (items == null || items.Length == 0)
            {
                return emptyList;
            }

            return items.ToList();
        }

        public static List<SearchResultItem> GetSearchResults([NotNull] this Item item, Func<SearchResultItem, bool> searchQuery)
        {
            var index = ContentSearchManager.GetIndex((SitecoreIndexableItem)item);
            var context = index.CreateSearchContext();

            using (context)
            {
                var results = context.GetQueryable<SearchResultItem>().Where(searchQuery).ToList();

                return results;
            }
        }

        public static List<Item> GetSearchItems([NotNull] this Item item, Func<SearchResultItem, bool> searchQuery)
        {
            var index = ContentSearchManager.GetIndex((SitecoreIndexableItem)item);
            var context = index.CreateSearchContext();

            using (context)
            {
                var results = context.GetQueryable<SearchResultItem>().Where(searchQuery);
                var itemList = results.Select(i => i.GetItem()).ToList();

                return itemList;
            }
        }


        public static IQueryable Where<T>(this IProviderSearchContext context, Func<T, bool> something)
        {
            using (context)
            {
                return context.GetQueryable<T>().Where(something).AsQueryable();
            }
        }

        public static IProviderSearchContext Search(this Item item)
        {
            if (item == null)
            {
                return null;
            }

            var index = ContentSearchManager.GetIndex((SitecoreIndexableItem)item);

            return index.CreateSearchContext();
        }

        public static IProviderSearchContext Search()
        {
            var item = (SitecoreIndexableItem)Sitecore.Context.Item;
            var index = ContentSearchManager.GetIndex(item);

            return index.CreateSearchContext();
        }

        /******************************GET ALL BASE TEMPLATES *********************************/
        public static List<string> GetAllParentTemplates(Item item)
        {
            if (item.Template == null) return null;

            List<string> list = new List<string>() { IdHelper.NormalizeGuid(item.TemplateID) };

            List<TemplateItem> baseTemplates = new List<TemplateItem>();
            GatherBaseTemplateItems(baseTemplates, item.Template, true);
            list.AddRange(baseTemplates.Select(i => IdHelper.NormalizeGuid(i.ID)).Distinct().ToList());
            return list;
        }


        public static void GatherBaseTemplateItems(List<TemplateItem> baseTemplates, TemplateItem templateItem, bool includeAncestors)
        {
            if (includeAncestors)
            {
                foreach (TemplateItem baseTemplateItem in templateItem.BaseTemplates)
                {
                    GatherBaseTemplateItems(baseTemplates, baseTemplateItem, includeAncestors);
                }
            }

            if (!IsStandardTemplate(templateItem) && templateItem.BaseTemplates != null && templateItem.BaseTemplates.Any())
            {
                baseTemplates.AddRange(GetBaseTemplatesExcludeStandardTemplate(templateItem.BaseTemplates));
            }
        }

        public static IEnumerable<TemplateItem> GetBaseTemplatesExcludeStandardTemplate(TemplateItem templateItem)
        {
            if (templateItem == null)
            {
                return new List<TemplateItem>();
            }

            return GetBaseTemplatesExcludeStandardTemplate(templateItem.BaseTemplates);
        }

        public static IEnumerable<TemplateItem> GetBaseTemplatesExcludeStandardTemplate(IEnumerable<TemplateItem> baseTemplates)
        {
            if (baseTemplates != null && baseTemplates.Any())
            {
                return baseTemplates.Where(baseTemplate => !IsStandardTemplate(baseTemplate));
            }

            return baseTemplates;
        }

        public static bool IsStandardTemplate(TemplateItem templateItem)
        {
            return templateItem.ID == TemplateIDs.StandardTemplate;
        }

        public static bool EvaluateRule(string fieldName, Item item, Item currentPage)
        {
            var ruleContext = new RuleContext();
            ruleContext.Item = currentPage;
            foreach (Rule<RuleContext> rule in RuleFactory.GetRules<RuleContext>(new[] { item }, fieldName).Rules)
            {
                if (rule.Condition != null)
                {
                    var stack = new RuleStack();
                    rule.Condition.Evaluate(ruleContext, stack);

                    if (ruleContext.IsAborted)
                    {
                        continue;
                    }
                    if ((stack.Count != 0) && ((bool)stack.Pop()))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    
}
}

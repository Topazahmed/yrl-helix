﻿using System;
using System.Linq;
using System.Web;

namespace Fishtank.Foundation.Helpers
{
    public static class RequestHelper
    {
        public static String GetUserIp()
        {
            var ipAddr = WasRequestForwarded() ? HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] : HttpContext.Current.Request.UserHostAddress;

            if (ipAddr == null || String.IsNullOrWhiteSpace(ipAddr)) return String.Empty;

            // X-Forwarded-For can contain multiple values. Example: client, proxy1, proxy2. The first value is best.
            if (ipAddr.Contains(",")) ipAddr = ipAddr.Split(',').First();

            return ipAddr.Trim();
        }

        public static bool WasRequestForwarded()
        {
            var forwardedFor = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            return forwardedFor != null && !String.IsNullOrWhiteSpace(forwardedFor);
        }
    }
}

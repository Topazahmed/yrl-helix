﻿using System;
using System.Linq;
using System.Web.Mvc;
using Fishtank.Foundation.Helpers;
using Sitecore.Mvc.Names;
using Sitecore.Mvc.Presentation;

namespace FFF.Feature.Redirect.Controllers
{
    public class RedirectController : Controller
    {
        // GET: Default
        public ActionResult Redirect()
        {
            var item = Sitecore.Context.Item;
            var permanantRedirect = false;
            var url = String.Empty;
            if (item.Fields["Permanent Redirect"].Value == "1")
            {
                permanantRedirect = true;
            }
            if (item.Fields["Redirect to Child"].Value == "1" && item.Children.Count > 0)
            {
                url = item.Children.First().GetUrl();
            }
            else
            {
                url = item.GetUrlFromLinkField("Redirect Url");
            }
            //if url is still empty, let it blow up
            return new RedirectResult(url, permanantRedirect);
        }
    }
}
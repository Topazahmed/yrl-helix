﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using System.Web.Mvc;
using Fishtank.Foundation.Helpers;
using Sitecore.Mvc.Presentation;

namespace FFF.Feature.RichText.Models
{
    public class RichText
    {
        public HtmlString Preheading { get; set; }
        public HtmlString Heading { get; set; }
        public HtmlString Subheading { get; set; }
        public HtmlString Body { get; set; }
        public HtmlString BackgroundImage { get; set; }
        public HtmlString BackgroundImageRaw { get; set; }
        public bool HasBackgroundImage { get; set; }
        public HtmlString Button1Text { get; set; }
        public HtmlString Button1Link { get; set; }
        public HtmlString Button1LinkUrl { get; set; }
        public HtmlString Button2Text { get; set; }
        public HtmlString Button2Link { get; set; }
        public HtmlString Button2LinkUrl { get; set; }

        public Parameters Parameters { get; set; }

        private Item _item;
        private RenderingParameters _renderingParameters;
       
        public RichText(Item item, RenderingParameters renderingParameters)
        {
            _item = item;
            _renderingParameters = renderingParameters;

            this.Preheading = new HtmlString(FieldRenderer.Render(_item, "Preheading"));            
            this.Heading = new HtmlString(FieldRenderer.Render(_item, "Heading"));            
            this.Subheading = new HtmlString(FieldRenderer.Render(_item, "Subheading"));           
            this.Body = new HtmlString(FieldRenderer.Render(_item, "Body"));            
            this.BackgroundImage = new HtmlString(FieldRenderer.Render(_item, "Background Image"));            
            this.BackgroundImageRaw = new HtmlString(_item.GetImageUrl("Background Image"));
            this.HasBackgroundImage = !String.IsNullOrWhiteSpace(_item["Background Image"]);
            this.Button1Text = new HtmlString(FieldRenderer.Render(_item, "Button 1 Text"));
            this.Button1Link = new HtmlString(FieldRenderer.Render(_item, "Button 1 Link"));
            this.Button1LinkUrl = new HtmlString(_item.GetUrlFromLinkField("Button 1 Link"));
            this.Button2Text = new HtmlString(FieldRenderer.Render(_item, "Button 2 Text"));
            this.Button2Link = new HtmlString(FieldRenderer.Render(_item, "Button 2 Link"));
            this.Button2LinkUrl = new HtmlString(_item.GetUrlFromLinkField("Button 2 Link"));

            var parameters = new Parameters();

            parameters.ColorTreatment = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Color Treatment"]);
            parameters.ContentPosition = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Content Position Horizontal"]);
            parameters.ContentWidth = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Content Width"]);
            parameters.Padding = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Padding"]);
            parameters.TextAlignment = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Text Alignment"]);

            this.Parameters = parameters;            
        }
    }

    public class Parameters
    {
        public string Padding { get; set; }
        public string TextAlignment { get; set; }
        public string ColorTreatment { get; set; }
        public string ContentWidth { get; set; }
        public string ContentPosition { get; set; }
    }
}
﻿using System.Web.Mvc;
using FFF.Feature.RichText.Models;
using Sitecore.Mvc.Presentation;
using static FFF.Feature.RichText.Models.RichText;

namespace FFF.Feature.RichText.Controllers
{
    public class RichTextController : Controller
    {
        public ActionResult RichText()
        {
            var richText = new FFF.Feature.RichText.Models.RichText(RenderingContext.Current.Rendering.Item, RenderingContext.Current.Rendering.Parameters);
            return View(richText);
        }
    }
}

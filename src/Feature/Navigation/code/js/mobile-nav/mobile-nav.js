﻿require(["cash", "lib/mobile-menu"], function (c$, MobileNav) {
    c$(function () {
        
        var mobileNav = new MobileNav(
            {
                "selectors": {
                    "openButton": ".open-menu-button",
                    "closeButton": ".close-button",
                    "mobileMenuPanel": ".mobile-nav"
                },
                "class" : {
                    "openState": "mobile-nav-is-open"                    
                }
            }
        );
    });
});

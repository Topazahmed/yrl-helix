﻿using System.Linq;
using System.Web.Mvc;
using FFF.Feature.Navigation.Repositories;
using Sitecore.Mvc.Presentation;
using Sitecore.Data;
using FFF.Feature.Navigation.Models;


namespace FFF.Feature.Navigation.Controllers
{
    public class NavigationController : Controller
    {
        public ActionResult DesktopNav()
        {
            var desktopItem = Sitecore.Context.Database.GetItem(new ID(Guids.Assets.DesktopNavigation));

            var repository = new NavigationRepository();
            var navCollection = repository.GetNavigation(desktopItem, RenderingContext.Current.ContextItem, "Link");

            return View(navCollection);
        }

        public ActionResult SideNav()
        {
            var repository = new NavigationRepository();
            var currentItem = RenderingContext.Current.ContextItem;
            var parentItem = currentItem.Parent;
            var navCollection = repository.GetNavigation(parentItem, currentItem);

            return View(navCollection);            
        }

        public ActionResult FooterNav()
        {

            var repository = new FooterContentRepository();

            var footerMenuRoot = Sitecore.Context.Database.GetItem(
                    new ID(Guids.Assets.FooterMenu)
                );

            var footerContent = repository.GetFooterContent(footerMenuRoot);
            
            return View(footerContent);
        }


        public ActionResult MobileNav()
        {
            var mobileItem = Sitecore.Context.Database.GetItem(new ID(Guids.Assets.MobileNavigation));

            var repository = new NavigationRepository();
            var navCollection = repository.GetNavigation(mobileItem, RenderingContext.Current.ContextItem, "Link");

            return View(navCollection);
        }

        public ActionResult BreadcrumbNav()
        {
            var repository = new NavigationRepository();
            var breadcrumb = repository.GetBreadcrumb(Sitecore.Context.Item);
            return View(breadcrumb);
        }
    }
}

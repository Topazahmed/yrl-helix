﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FFF.Feature.Navigation.Models;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace FFF.Feature.Navigation.Repositories
{
    public class NavigationRepository : INavigationRepository
    {
        public NavigationCollection GetNavigation(Item rootItem, Item contextItem, string linkFieldName = null)
        {
            if (rootItem == null) return null;

            return new NavigationCollection(rootItem, contextItem, linkFieldName);
        }

        public Breadcrumb GetBreadcrumb (Item contextItem)
        {
            return contextItem != null ? new Breadcrumb(contextItem) : null;
        }
    }
}

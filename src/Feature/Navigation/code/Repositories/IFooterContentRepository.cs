﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFF.Feature.Navigation.Models;
using Sitecore.Data.Items;
using Sitecore.Diagnostics.Events;

namespace FFF.Feature.Navigation.Repositories
{
    interface IFooterContentRepository
    {
        FooterContent GetFooterContent(Item item);
    }
}

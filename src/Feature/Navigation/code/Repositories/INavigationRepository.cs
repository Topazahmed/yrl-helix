﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFF.Feature.Navigation.Models;
using Sitecore.Data.Items;

namespace FFF.Feature.Navigation.Repositories
{
    interface INavigationRepository
    {
        NavigationCollection GetNavigation(Item rootItem, Item contextItem, string linkFieldName);

        Breadcrumb GetBreadcrumb(Item contextItem);
    }
}

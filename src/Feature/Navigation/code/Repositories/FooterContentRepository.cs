﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FFF.Feature.Navigation.Models;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace FFF.Feature.Navigation.Repositories
{
    public class FooterContentRepository : IFooterContentRepository
    {
        public FooterContent GetFooterContent(Item item)
        {
            Assert.IsNotNull(item, "Item for Footer content is null.");

            var footerItems = item.Children.ToList();
            var footerContentItems = footerItems.Select(FooterContentItem.Create);

            var footerContent = new FooterContent(item);
            footerContent.FooterContentItems = footerContentItems;

            return footerContent;            
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FFF.Feature.Navigation
{
    public class Guids
    {
        public static class Pages
        {
            public static string HomePage = "{05465FAE-7B90-4C8D-BA5D-F8A1540F1337}";
        }

        public static class Assets
        {
            public static string DesktopNavigation = "{38FEC149-7FE1-49C2-B433-B759142924C2}";
            public static string MobileNavigation = "{DEF905AD-B97B-4254-9F68-2C71D47DE7D2}";
            public static string FooterMenu = "{B5802E2C-9402-4DFB-879F-438FAF9974FB}";            
        }

        public static class Renderings
        {
            public static string DesktopNavigation = "{01825DFA-DB66-4DC9-9076-942688174C7B}";
            public static string MobileNavigation = "{C835ABD2-2F25-4E82-9941-A019768FD00A}";
            public static string Breadcrumb = "{C0A2648B-7DA7-4F98-9579-91A03393C3CA}";
            public static string SideNavigation = "{C4119E7A-A9D1-455C-B6EC-8987E33CEBEE}";
            public static string Footer = "{21EC0889-B25E-40A5-A832-04F634A1955D}";
        }
    }
}
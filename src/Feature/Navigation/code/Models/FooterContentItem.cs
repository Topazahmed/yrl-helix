﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Web.UI.WebControls;

namespace FFF.Feature.Navigation.Models
{
    public class FooterContentItem
    {
        private Item _item;

        public string Body { get; set; }
        public string ExtraClasses { get; set; }
        public string ExtraClassesRaw { get; set; }


        public FooterContentItem(Item item)
        {
            Sitecore.Diagnostics.Assert.IsNotNull(item, "FooterContentItem Model is null");

            _item = item;

            Body = FieldRenderer.Render(_item, "Body");
            ExtraClasses = FieldRenderer.Render(_item, "Extra Classes");
            ExtraClassesRaw = _item["Extra Classes"];
        }

        public static FooterContentItem Create(Item item)
        {
            return new FooterContentItem(item);           
        }
    }
}
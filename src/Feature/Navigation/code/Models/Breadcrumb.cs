﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using Fishtank.Foundation.Helpers;
using Sitecore.ContentSearch;

namespace FFF.Feature.Navigation.Models
{
    public class Breadcrumb
    {
        public Item Item { get; set; }
        public List<NavItem> Items { get; set; }
        public Item HomeItem { get; set; }
        public bool IsCurrent { get; set; }
        public bool IsActive { get; set; }

        public Breadcrumb(Item contextItem)
        {
            Item = contextItem;
            HomeItem = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(Guids.Pages.HomePage));
            Items = GetCrumbs();
        }

        private List<NavItem> GetCrumbs()
        {
            var crumbs = new List<NavItem>();

            if (Item == null || HomeItem == null) return crumbs;

            var ancestors = Item.Axes.GetAncestors();

            if (ancestors == null) return crumbs;

            var ancestorList = ancestors.ToList();

            if (ancestorList != null && ancestorList.Any())
            {
                ancestorList.Add(Item); // add current item to end
                ancestorList = ancestorList.Where(i => i.Axes.IsDescendantOf(HomeItem)).ToList(); // scope to home item

                foreach (var i in ancestorList)
                {
                    if (String.IsNullOrWhiteSpace(i["Heading"])) { continue; }

                    if (!ItemHelper.EvaluateRule("Breadcrumb Rules", Item, i))
                    {
                        continue;
                    }

                    var navItem = new NavItem();
                    navItem.Item = i;
                    navItem.IsCurrent = (i == Item ? true : false); 
                    crumbs.Add(navItem);
                }
            }

            return crumbs;
        }
    }
}

﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FFF.Feature.Navigation.Models
{
    public class NavItem
    {
        public Item Item { get; set; }
        public bool IsCurrent { get; set; }
        public bool IsActive { get; set; }
    }
}
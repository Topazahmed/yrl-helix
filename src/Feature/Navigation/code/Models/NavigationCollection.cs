﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Data.Fields;

namespace FFF.Feature.Navigation.Models
{
    public class NavigationCollection
    {
        private Dictionary<ID, List<NavItem>> _relatedItems;
        private Item _contextItem;
        private String _contextPath;
        public List<NavItem> Items;
        public Item ParentItem;
        public bool IsHomePage;
        private string _linkFieldName;

        public NavigationCollection(Item rootItem, Item contextItem, String linkFieldName = null)
        {
            if (rootItem == null) return;
            if (contextItem == null) return;

            ParentItem = contextItem.Parent;

            _contextItem = contextItem;
            _contextPath = _contextItem.Paths.FullPath.ToLower();
            _linkFieldName = linkFieldName;
            _relatedItems = new Dictionary<ID, List<NavItem>>();

            var items = rootItem.GetChildren().ToList();
            Items = items.Select(CreateNavItem).ToList();
            
            var descendants = rootItem.Axes.GetDescendants();
            BuildRelatedItems(descendants);

            IsHomePage = SetIsHomePage(_contextItem);
        }
        
        private bool SetIsHomePage(Item item)
        {
            if (Sitecore.Context.Site == null) return false;
            if (item == null) return false;

            return item.Paths.FullPath.ToLower() == Sitecore.Context.Site.StartPath.ToLower();
        }
        private void BuildRelatedItems(Item[] descendants)
        {
            foreach(Item i in descendants)
            {
                var id = i.ParentID;
                if(_relatedItems.ContainsKey(id))
                {
                    var items = _relatedItems[id];
                    items.Add(CreateNavItem(i));
                    _relatedItems[id] = items;
                } else
                {
                    var tempList = new List<NavItem>();
                    tempList.Add(CreateNavItem(i));
                    _relatedItems.Add(id, tempList);
                }
            }
        }

        private NavItem CreateNavItem(Item item)
        {
            var navItem = new NavItem();
            navItem.Item = item;
            navItem.IsActive = IsActive(item);
            navItem.IsCurrent = IsCurrent(item);
            return navItem;
        }

        /// <summary>
        /// Determines if we're checking items themselves or a field in item for their
        /// location
        /// </summary>
        /// <returns></returns>
        private bool UseItemForPath()
        {
            return String.IsNullOrWhiteSpace(_linkFieldName);
        }

        private bool IsActive(Item item)
        {
            if (item == null) return false;

            var path = GetPathFromItem(item);
            
            return _contextPath.Contains(path);
        }

        private bool IsCurrent(Item item)
        {
            if (item == null) return false;
            
            var path = GetPathFromItem(item);
            return path == _contextPath;
        }        

        private string GetPathFromItem(Item item) {

            if (item == null) return String.Empty;

            if(UseItemForPath()) return item.Paths.FullPath.ToLower();            

            var field = item.Fields[_linkFieldName];
            //new Sitecore.Data.Fields
            if (field != null && 
                field.Type != null && 
                !field.Type.ToLower().Contains("general link")) return String.Empty;

            var linkField = new LinkField(field);

            if (!linkField.IsInternal) return String.Empty;

            var targetItem = linkField.TargetItem;            

            if (targetItem == null) return String.Empty;

            return targetItem.Paths.FullPath.ToLower();            
        }

        public List<NavItem> GetRelatedItems(ID id)
        {            
            if (!_relatedItems.ContainsKey(id)) return new List<NavItem>();

            var items = _relatedItems[id];
            return items;
        }
    }
}
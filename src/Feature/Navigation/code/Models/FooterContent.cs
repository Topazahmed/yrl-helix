﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fishtank.Foundation.Helpers;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

namespace FFF.Feature.Navigation.Models
{
    public class FooterContent
    {
        public IEnumerable<FooterContentItem> FooterContentItems { get; set; }
        public string CopyrightText { get; set; }
        public string CopyrightTextRaw { get; set; }
        public string ColorTreatment { get; set; }
        public string Padding { get; set; }

        private Item _item;

        public FooterContent(Item item)
        {
            _item = item;
            FooterContentItems = new List<FooterContentItem>();
            CopyrightText = FieldRenderer.Render(_item,"Copyright Text");
            ColorTreatment = ViewHelper.GetParameterDropLinkValue(_item["Color Treatment"]);
            Padding = ViewHelper.GetParameterDropLinkValue(_item["Padding"]);
        }
    }
}
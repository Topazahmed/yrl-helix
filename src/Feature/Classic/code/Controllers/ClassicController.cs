﻿using System.Web.Mvc;
using FFF.Feature.Classic.Models;
using Sitecore.Mvc.Presentation;
using static FFF.Feature.Classic.Models.Classic;

namespace FFF.Feature.Classic.Controllers
{
    public class ClassicController : Controller
    {
        public ActionResult Classic()
        {
            var richText = new FFF.Feature.Classic.Models.Classic(RenderingContext.Current.Rendering.Item, RenderingContext.Current.Rendering.Parameters);
            return View(richText);
        }
    }
}

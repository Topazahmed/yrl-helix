﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using System.Web.Mvc;
using Fishtank.Foundation.Helpers;
using Sitecore.Mvc.Presentation;

namespace FFF.Feature.Classic.Models
{
    public class Classic
    {
        public string Preheading { get; set; }
        public string Heading { get; set; }
        public string Subheading { get; set; }
        public string Body { get; set; }
        public string Image { get; set; }
        public string ImageRaw { get; set; }
        public bool HasImage { get; set; }

        public string BackgroundImage { get; set; }
        public string BackgroundImageRaw { get; set; }
        public bool HasBackgroundImage { get; set; }
        public string Button1Text { get; set; }
        public string Button1Link { get; set; }
        public string Button1LinkUrl { get; set; }
        public string Button2Text { get; set; }
        public string Button2Link { get; set; }
        public string Button2LinkUrl { get; set; }

        public Parameters Parameters { get; set; }

        private Item _item;
        private RenderingParameters _renderingParameters;

        public Classic(Item item, RenderingParameters renderingParameters)
        {
            _item = item;
            _renderingParameters = renderingParameters;

            this.Preheading = FieldRenderer.Render(_item, "Preheading");
            this.Heading = FieldRenderer.Render(_item, "Heading");
            this.Subheading = FieldRenderer.Render(_item, "Subheading");
            this.Body = FieldRenderer.Render(_item, "Body");
            this.Image = FieldRenderer.Render(_item, "Image");
            this.BackgroundImage = FieldRenderer.Render(_item, "Background Image");
            this.BackgroundImageRaw = _item.GetImageUrl("Background Image");
            this.HasImage = !String.IsNullOrWhiteSpace(_item["Image"]);
            this.HasBackgroundImage = !String.IsNullOrWhiteSpace(_item["Background Image"]);

            this.Button1Text = FieldRenderer.Render(_item, "Button 1 Text");
            this.Button1Link = FieldRenderer.Render(_item, "Button 1 Link");
            this.Button1LinkUrl = _item.GetUrlFromLinkField("Button 1 Link");
            this.Button2Text = FieldRenderer.Render(_item, "Button 2 Text");
            this.Button2Link = FieldRenderer.Render(_item, "Button 2 Link");
            this.Button2LinkUrl = _item.GetUrlFromLinkField("Button 2 Link");

            var parameters = new Parameters();

            parameters.ColorTreatment = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Color Treatment"]);
            parameters.ContentPosition = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Content Position Horizontal"]);
            parameters.ContentWidth = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Content Width"]);
            parameters.Padding = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Padding"]);
            parameters.TextAlignment = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Text Alignment"]);
            parameters.ImageAlignment = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Image Alignment"]);

            this.Parameters = parameters;
        }
    }

    public class Parameters
    {
        public string Padding { get; set; }
        public string TextAlignment { get; set; }
        public string ImageAlignment { get; set; }

        public string ColorTreatment { get; set; }
        public string ContentWidth { get; set; }
        public string ContentPosition { get; set; }
    }
}
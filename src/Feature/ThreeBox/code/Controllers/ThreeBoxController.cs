﻿using System.Web.Mvc;
using Sitecore.Mvc.Presentation;

namespace FFF.Feature.ThreeBox.Controllers
{
    public class ThreeBoxController : Controller
    {
        public ActionResult ThreeBox()
        {
            var threeBox = new FFF.Feature.ThreeBox.Models.ThreeBox(RenderingContext.Current.Rendering.Item, RenderingContext.Current.Rendering.Parameters);
            return View(threeBox);
        }
    }
}

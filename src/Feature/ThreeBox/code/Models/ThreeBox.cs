﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using System.Web.Mvc;
using Fishtank.Foundation.Helpers;
using Sitecore.Mvc.Presentation;

namespace FFF.Feature.ThreeBox.Models
{
    public class ThreeBox
    {
        public HtmlString Preheading { get; set; }
        public HtmlString Heading { get; set; }
        public HtmlString Subheading { get; set; }
        public HtmlString Body { get; set; }
        public HtmlString BackgroundImage { get; set; }
        public HtmlString BackgroundImageRaw { get; set; }
        public bool HasBackgroundImage { get; set; }
        public HtmlString FeaturedImage { get; set; }
        public HtmlString FeaturedImageRaw { get; set; }
        public bool HasFeaturedImage { get; set; }
        public HtmlString Button1Text { get; set; }
        public HtmlString Button1Link { get; set; }
        public HtmlString Button1LinkUrl { get; set; }
        public HtmlString Button2Text { get; set; }
        public HtmlString Button2Link { get; set; }
        public HtmlString Button2LinkUrl { get; set; }

        public HtmlString TopSmallBoxHeading { get; set; }
        public HtmlString TopSmallBoxBody { get; set; }
        public HtmlString BottomSmallBoxHeading { get; set; }
        public HtmlString BottomSmallBoxBody { get; set; }

        public Parameters Parameters { get; set; }

        private Item _item;
        private RenderingParameters _renderingParameters;
       
        public ThreeBox(Item item, RenderingParameters renderingParameters)
        {
            _item = item;
            _renderingParameters = renderingParameters;
        
            this.Heading = new HtmlString(FieldRenderer.Render(_item, "Heading"));                      
            this.Body = new HtmlString(FieldRenderer.Render(_item, "Body"));            
            this.BackgroundImage = new HtmlString(FieldRenderer.Render(_item, "Background Image"));            
            this.BackgroundImageRaw = new HtmlString(_item.GetImageUrl("Background Image"));
            this.HasBackgroundImage = !String.IsNullOrWhiteSpace(_item["Background Image"]);

            this.FeaturedImage = new HtmlString(FieldRenderer.Render(_item, "Featured Image"));
            this.FeaturedImageRaw = new HtmlString(_item.GetImageUrl("Featured Image"));
            this.HasFeaturedImage = !String.IsNullOrWhiteSpace(_item["Featured Image"]);

            this.Button1Text = new HtmlString(FieldRenderer.Render(_item, "Button 1 Text"));
            this.Button1Link = new HtmlString(FieldRenderer.Render(_item, "Button 1 Link"));
            this.Button1LinkUrl = new HtmlString(_item.GetUrlFromLinkField("Button 1 Link"));
            this.Button2Text = new HtmlString(FieldRenderer.Render(_item, "Button 2 Text"));
            this.Button2Link = new HtmlString(FieldRenderer.Render(_item, "Button 2 Link"));
            this.Button2LinkUrl = new HtmlString(_item.GetUrlFromLinkField("Button 2 Link"));

            this.TopSmallBoxHeading = new HtmlString(FieldRenderer.Render(_item, "Top Small Box Heading"));
            this.TopSmallBoxBody = new HtmlString(FieldRenderer.Render(_item, "Top Small Box Body"));

            this.BottomSmallBoxHeading = new HtmlString(FieldRenderer.Render(_item, "Bottom Small Box Heading"));
            this.BottomSmallBoxBody = new HtmlString(FieldRenderer.Render(_item, "Bottom Small Box Body"));

            var parameters = new Parameters();

            parameters.ColorTreatmentBackground = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Color Treatment Background"]);
            parameters.ColorTreatmentMainBox = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Color Treatment Main Box"]);
            parameters.ColorTreatmentTopSmallBox = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Color Treatment Top Small Box"]);
            parameters.ColorTreatmentBottomSmallBox = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Color Treatment Bottom Small Box"]);
            parameters.MainBoxPositionHorizontal = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Main Box Position Horizontal"]);
            parameters.Padding = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Padding"]);
            parameters.TextAlignment = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Text Alignment"]);

            this.Parameters = parameters;            
        }
    }

    public class Parameters
    {
        public string Padding { get; set; }
        public string TextAlignment { get; set; }
        public string ColorTreatmentBackground { get; set; }
        public string ColorTreatmentMainBox { get; set; }
        public string ColorTreatmentTopSmallBox { get; set; }
        public string ColorTreatmentBottomSmallBox { get; set; }
        public string MainBoxPositionHorizontal { get; set; }
    }
}
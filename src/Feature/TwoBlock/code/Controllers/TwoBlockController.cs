﻿using System.Web.Mvc;
using FFF.Feature.TwoBlock.Models;
using Sitecore.Mvc.Presentation;
using static FFF.Feature.TwoBlock.Models.TwoBlock;

namespace FFF.Feature.TwoBlock.Controllers
{
    public class TwoBlockController : Controller
    {
        public ActionResult TwoBlock()
        {
            var twoBlock = new FFF.Feature.TwoBlock.Models.TwoBlock(RenderingContext.Current.Rendering.Item, RenderingContext.Current.Rendering.Parameters);
            return View(twoBlock);
        }
    }
}

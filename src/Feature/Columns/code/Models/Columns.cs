﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using System.Web.Mvc;
using Fishtank.Foundation.Helpers;
using Sitecore.Mvc.Presentation;

namespace FFF.Feature.Columns.Models
{
    public class Columns
    {
        public HtmlString Preheading { get; set; }
        public HtmlString Heading { get; set; }
        public HtmlString Subheading { get; set; }
        public HtmlString Body { get; set; }
        public HtmlString BackgroundImage { get; set; }
        public HtmlString BackgroundImageRaw { get; set; }
        public bool HasBackgroundImage { get; set; }
        public HtmlString FeaturedImage { get; set; }
        public HtmlString FeaturedImageRaw { get; set; }
        public bool HasFeaturedImage { get; set; }

        public Parameters Parameters { get; set; }

        public List<Column> ColumnItems { get; set; }

        private Item _item;
        private RenderingParameters _renderingParameters;
       
        public Columns(Item item, RenderingParameters renderingParameters)
        {
            _item = item;
            _renderingParameters = renderingParameters;
        
            this.Preheading = new HtmlString(FieldRenderer.Render(_item, "Preheading"));
            this.Heading = new HtmlString(FieldRenderer.Render(_item, "Heading"));
            this.Subheading = new HtmlString(FieldRenderer.Render(_item, "Subheading"));

            this.Body = new HtmlString(FieldRenderer.Render(_item, "Body"));            
            this.BackgroundImage = new HtmlString(FieldRenderer.Render(_item, "Background Image"));            
            this.BackgroundImageRaw = new HtmlString(_item.GetImageUrl("Background Image"));
            this.HasBackgroundImage = !String.IsNullOrWhiteSpace(_item["Background Image"]);

            this.FeaturedImage = new HtmlString(FieldRenderer.Render(_item, "Featured Image"));
            this.FeaturedImageRaw = new HtmlString(_item.GetImageUrl("Featured Image"));
            this.HasFeaturedImage = !String.IsNullOrWhiteSpace(_item["Featured Image"]);

            var parameters = new Parameters();

            parameters.ColorTreatment = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Color Treatment"]);
            parameters.Padding = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Padding"]);
            parameters.TextAlignment = ViewHelper.GetParameterDropLinkValue(_renderingParameters["Text Alignment"]);

            this.Parameters = parameters;

            var columnItems = ItemHelper.GetItemsFromListField("Columns", _item);

            this.ColumnItems = columnItems.Select(x => new Column(x)).ToList();
        }
    }

    public class Parameters
    {
        public string Padding { get; set; }
        public string TextAlignment { get; set; }
        public string ColorTreatment { get; set; }
    }

    public class Column
    {
        public HtmlString FeaturedImage { get; set; }
        public HtmlString FeaturedImageRaw { get; set; }
        public bool HasFeaturedImage { get; set; }
        public HtmlString Heading { get; set; }
        public HtmlString Body { get; set; }

        public HtmlString ButtonText { get; set; }
        public HtmlString ButtonLink { get; set; }
        public HtmlString ButtonLinkUrl { get; set; }

        public Column(Item columnItem)
        {
            this.FeaturedImage = new HtmlString(FieldRenderer.Render(columnItem, "Featured Image"));
            this.FeaturedImageRaw = new HtmlString(columnItem.GetImageUrl("Featured Image"));
            this.HasFeaturedImage = !String.IsNullOrWhiteSpace(columnItem["Featured Image"]);

            this.Heading = new HtmlString(FieldRenderer.Render(columnItem, "Heading"));
            this.Body = new HtmlString(FieldRenderer.Render(columnItem, "Body"));

            this.ButtonText = new HtmlString(FieldRenderer.Render(columnItem, "Button Text"));
            this.ButtonLink = new HtmlString(FieldRenderer.Render(columnItem, "Button Link"));
            this.ButtonLinkUrl = new HtmlString(columnItem.GetUrlFromLinkField("Button Link"));
        }
    }
}
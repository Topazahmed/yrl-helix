﻿using System.Web.Mvc;
using Sitecore.Mvc.Presentation;

namespace FFF.Feature.Columns.Controllers
{
    public class ColumnsController : Controller
    {
        public ActionResult Columns()
        {
            var columns = new FFF.Feature.Columns.Models.Columns(RenderingContext.Current.Rendering.Item, RenderingContext.Current.Rendering.Parameters);
            return View(columns);
        }
    }
}

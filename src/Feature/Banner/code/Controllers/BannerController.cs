﻿using System.Web.Mvc;
using Sitecore.Mvc.Presentation;

namespace code.Controllers
{
    public class BannerController : Controller
    {
        // GET: Default
        public ActionResult Banner()
        {
            var banner = new FFF.Feature.Banner.Models.Banner(RenderingContext.Current.Rendering.Item, RenderingContext.Current.Rendering.Parameters);

            return View(banner);
        }
    }
}
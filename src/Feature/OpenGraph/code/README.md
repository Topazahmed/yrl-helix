﻿# Open Graph Feature

To add this feature to your project: 

1. Add the following code to inside of the head of the main layout:

```
@Html.Partial("~/Views/OpenGraph/_OpenGraph.cshtml")
```

2. Sync the TDS items into your Templates > Features folder.

3. Add the section template as an inherited item in your various page templates.

4. Add and remove fields as necessary. Consult with the client to see which fields they want / need.
﻿requirejs.config({
    //To get timely, correct error triggers in IE, force a define/shim exports check.
    "baseUrl": "/js/",
    //enforceDefine: true,
    "paths": {
        "lib": "lib",
        "v": "vendor",
        "cash": "vendor/cash.min",
        "reqwest": "vendor/reqwest"
    }
});
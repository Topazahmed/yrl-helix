﻿require(["cash", "lib/scroll-intent"], function (c$, ScrollIntent) {
    c$(function () {

        var $window = c$(window);

        var scrollIntent = new ScrollIntent({
            "topOfPageDeadzone": { "top": 0, "bottom": 60 }
        });

        $window.on("movingUp", function (e) {
            c$("body").removeClass("nav-hidden");
        });

        $window.on("movingDown", function (e) {
            c$("body").addClass("nav-hidden");
        });

    });
});

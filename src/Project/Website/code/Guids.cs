﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FFF.Website
{
    public static class Guids
    {
        public static class Pages
        {
//            public static string HomePage = "{BDDAC79F-45A2-45A2-9868-4BB19DED9478}";
        }

        public static class Assets
        {
//            public static string DesktopNavigation = "{743011C0-87AE-426D-82E3-547EBD72AA42}";
//            public static string MobileNavigation  = "{6F03C37A-D219-4EF9-9093-5D7D13779EA6}";
//            public static string Footer            = "{FB07FA17-3DC3-4E60-9057-B52D29485A61}";
        }

        public static class Config
        {
//            public static string MetaTitleSuffix = "{6504E10A-0FE4-484F-B874-1795509341CC}";
//            public static string NewsFeedRssUrl = "{C48AF478-540C-4C1E-AD5E-A2647B090493}";
        }

        public static class Renderings
        {
            public static string DesktopNavigation = "{01825DFA-DB66-4DC9-9076-942688174C7B}";
            public static string MobileNavigation  = "{C835ABD2-2F25-4E82-9941-A019768FD00A}";
            public static string Breadcrumb        = "{C0A2648B-7DA7-4F98-9579-91A03393C3CA}";
            public static string SideNavigation    = "{C4119E7A-A9D1-455C-B6EC-8987E33CEBEE}";
            public static string Footer            = "{21EC0889-B25E-40A5-A832-04F634A1955D}";
        //    public static string LatestNewsContent = "{0C0F8051-870F-4AE6-B9F9-4BBEB5A0BEAD}";
        }

        public static class Templates
        {        
        }
    }
}

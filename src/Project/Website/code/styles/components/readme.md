# Components

Contains styling for universal elements used through out the site, within other functionality:

- buttons
- toggles
- form inputs
- spinners

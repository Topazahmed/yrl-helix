# Base

The folder contains base styling to be used through the site.  For example:

- Fonts
- Colors & themes
- Spacing rules
- Positioning rules

It would *not* contain elements such as

- Buttons
- Navigation
- Components

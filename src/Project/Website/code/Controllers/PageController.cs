﻿using System.Web.Mvc;
using Sitecore.Mvc.Presentation;

namespace FFF.Website.Controllers
{
    public class PageController : Controller
    {
        public ActionResult ContentPage()
        {
            var contentPage = RenderingContext.Current.Rendering.Item;
            return View("/Views/Pages/ContentPage.cshtml", contentPage);
        }
    }
}
